# Use on a host running Ubuntu Xenial (16.04.x) and has Python 2.7.
FROM ubuntu:xenial
FROM python:2.7

# Required utilities and packages.
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y binutils

# Install sqlite3 (required for Greengrass).
RUN apt-get install -y sqlite3

# Install wget (required for Greengrass).
RUN apt-get install -y wget

# For Greengrass.
RUN adduser --system ggc_user
RUN addgroup --system ggc_group

# Add the shell script to the container
COPY compute_master.sh compute_master.sh

# Add the Greengrass Core Software image with the certificates and config file to the container.
COPY greengrass/ greengrass/

# Run the bash script when the container is started.
ENTRYPOINT ["/compute_master.sh"]
