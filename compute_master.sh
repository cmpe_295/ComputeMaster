#!/bin/bash

# Start the GGC daemon.
cd /greengrass/ggc/core
./greengrassd start

# To keep the container running.
tail -f /dev/null
