CMPE 295 ComputeMaster

Getting Started

This program builds the aws-greengrass image and runs the Greengrass Core in a Docker container on a fog node/client.

Prerequisites

The client must have Git installed to clone the repo and be able to use tar.

Installing

This repo is cloned when the install dependencies script is executed
(ecep_client_cpu/ecep_client/install_dependencies.sh).

The Dockerfile is executed by container.py
(ecep_client_cpu/ecep_client/ecep_endNode/ecep_wampClient) when the
aws-greengrass image is created in the ECEP dashboard. The Dockerfile executes
compute_master.sh which starts the Greengrass core daemon then does a tail -f
dev/null to keep the container from exiting.

Versioning

v1.0

Authors

Sona Bhasin
Kanti Bhat
Johnny Nigh
Monica Parmanand
